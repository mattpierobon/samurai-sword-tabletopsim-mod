
--[[ 
	Samurai Sword TableTop Simulator Mod
	Author: Cold, Trooper
	Date: 2023-04-22

	TableTop Documentation:
	https://api.tabletopsimulator.com/ 

	Samurai Sword Documentation: 
	Rules: https://www.fgbradleys.com/rules/rules2/SamuraiSword-rules.pdf
	Rising Sun Rules: https://www.dvgiochi.com/giochi/risingsun/download/rising_sun-rules.pdf
	

	Distributed under MIT Lisence which states: 
	
	The software is provided “as is”, without warranty of any kind, express or implied, 
	including but not limited to the warranties of merchantability, fitness for a 
	particular purpose and noninfringement. in no event shall the authors or copyright 
	holders be liable for any claim, damages or other liability, whether in an action of 
	contract, tort or otherwise, arising from, out of or in connection with the software 
	or the use or other dealings in the software.


	TODO::
		Scripting area for character cards, roles, health and shuriken (one area for each player [x7])

--]]


--[======[ Event Listeners ]======]

--[[ The onLoad event is called after the game save finishes loading. --]]
function onLoad()
	-- Start message
	printToAll("Welcome to Samurai Sword!")
    printToAll("Authors: Cold, Trooper")
    printToAll("Version: 0.1.0")
    printToAll("Distributed under MIT lisence")
    printToAll("")
	
    cmd_help()

	-- TODO
	-- Draw character, starting role, starting number of playing cards
	-- Enable "PlayerTurns" by default

end


--[[ The onPlayerTurn event at the beginning of every round --]]
function onPlayerTurn(player, previous_player)
	-- TODO
	-- Match each characters special ability to areas in the flow of a game
	-- Draw Phase
	-- Playing Phase
	-- 	Properties to be placed
	--	Actions to be actioned
	--	Attacks to be attacked
end


--[[ The onChat event is called anytime a non-command chat message is sent (return false to not display the message) --]]
function onChat(message,sender)
	if message == "\\start" then 
		cmd_start(sender)
		return false
	elseif message == "\\character" then 
		get_player_characters()
		return false
	elseif message == "\\help" then 
		cmd_help(sender)
		return false
	end

	return true
end


--[[ The onUpdate event is called once per frame. --]]
function onUpdate()
    --[[ print('onUpdate loop!') --]]
end



--[======[ Commands ]======]

function cmd_start(sender)
	printToAll(sender.get_steam_name() .. " started the game!\n", Color.Green)
	-- create_character_deck()

	create_decks()
end


function cmd_character()
	get_player_characters()
end


function cmd_help(sender)
	local msg = "Commands:\n"
	for _, cmd in pairs(command_map) do
		msg = msg .. cmd .. "\n"
	end

	if sender then
		printToColor(msg, sender.color)
	else 
		printToAll(msg)
	end
end



--[======[ Getters ]======]

function get_player_characters()
	for _, player in ipairs(Player.getPlayers()) do 	  
		local c_found = false
		for _, obj in ipairs(player.getHandObjects()) do   
			for _,tag in ipairs (obj.getTags()) do	       
				local c = get_character_from_tag(tag)
				if c ~= nil then
					c_found = true
					print_character_details(player,c)
					goto found
				end
			end
		end 
		::found::
		if not c_found then
			print("no character card found for " .. player.steam_name)
		end
	end
	return nil
end


function get_character_from_tag(tag)
	for key, character in pairs(character_map) do
		if character.name == tag then
			return character
		end
	end
	return nil
end


--[======[ Prints ]======]

function print_character_details(player, character)
	printToAll(player.steam_name .. " is playing as " .. character.name)
	printToAll(character.name .. "'s special ability is: " .. character.description)
	printToAll("")
end



--[======[ Game Objects ]======]

function create_character_deck()
	shuffle_table(character_map)
	for title,character in pairs(character_map) do 
		-- Spawn Default Object
		local card = spawnObject({
	    	type = "Card",
	    	position = {0,3,0},
	    	scale = {1,1,1},
	    	sound = false,
	    	callback_function = function(spawned_object)
	        	log(spawned_object.getBounds())
	    	end
		})
		card.setPositionSmooth({5, 5, 5})
		card.flip()

		-- Apply Custom Parameters 
		local custom_params = {
			type = 0,
			face = character.face_path,
			back = character_back_path,
			sideways = false
		}
		card.setCustomObject(custom_params) 

		-- Set Name and Description
		card.setName(title)
		card.setDescription(character.description)
		
		-- Apply Custom Tags
		card.addTag(character.name)

		-- Add to relevant GUID table
		table.insert(character_guid, card.getGUID())
	end
end


--[[ 
	Spawns in the 3 main game decks: roles, characters, and playing cards
]]
function create_decks()
	local decks = {
		role_map,
		shuffle_table(play_card_map),
		shuffle_table(character_map),
	}

	-- enable this and remove above decks variable to have #roles == #Players
	-- local decks = {
	-- 	{},
	-- 	shuffle_table(play_card_map),
	-- 	shuffle_table(character_map),
	-- }
	-- ensure # role cards == # players
	-- local player_count = #Player.getPlayers()
	-- local i = 1
	-- for _, role_data in pairs(role_map) do
	-- 	if i <= player_count then
	-- 		decks[1][i] = role_data
	-- 	end
	-- 	i = i + 1
	-- end

	local backs = {
		role_back_path,
		card_back_path,
		character_back_path,
	}

	for i, deck in pairs(decks) do
		local x_coord = (i - 2) * 3 -- so the decks spawn side by side
		for title, card_data in pairs(deck) do
			local card_count = 1
			if card_data.count then
				card_count = card_data.count
			end

			for j = 1, card_count do 
				-- Spawn Default Object
				local card = spawnObject({
					type = "Card",
					position = {x_coord,3,0},
					sound = false,
					callback_function = function(spawned_object)
						log(spawned_object.getBounds())
					end
				})
				card.setPositionSmooth({5, 5, 5})
				card.flip()

				-- Apply Custom Parameters 
				local custom_params = {
					type = 0,
					face = card_data.face_path,
					back = backs[i],
					sideways = false
				}
				card.setCustomObject(custom_params) 

				-- Set Name and Description
				card.setName(title)

				if card_data.description and card_data.description ~= "" then 
					card.setDescription(card_data.description)
				end

				-- Apply Custom Tags
				card.addTag(card_data.name)

				-- Add to relevant GUID table
				cards_by_guid[card.getGUID()] = card_data
			end
		end
	end

end

--[======[ Helpers/Misc ]======]

--[[ In-place fisher-yates --]]
function shuffle_table(t)
  	for i = #t, 2, -1 do
    	local j = math.random(i)
    	print(j)
    	t[i], t[j] = t[j], t[i]
  	end
  return t
end



--[======[ Globals ]======]

-- GUIDs
cards_by_guid = {}
character_guid = {}

-- Cards 
character_map = {
	-- Original Game
	Benkei = {
		name = "benkei",
		health = 5,
		description = "All other players attack you at +1 difficulty.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070033954/584BA5097E5E1672D63FE594020B7369B892F567/",
	},
	Chiyome = {
		name = "chiyome", 
		health = 4,
		description = "You can be only wounded by weapons.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034131/DFC38131D0806062032BF934A5401BA000072E79/",
	},
	Ginchiyo = {
		name = "ginchyio",
		health = 4,
		description = "You suffer 1 less wound from Weapons (minimum 1).",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034234/4604248B8AA86B545D4C33DEB9ED0C21853C25BC/",
	},
	Goemon = { 
		name = "goemon",
		health = 5,
		description = "You may play 1 additional weapon each turn.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034342/A26D1ED550665BE323CEFBD2CC7DAEC3281DCCC9/",
	},
	Hanzo = {
		name = "hanzo",
		health = 4,
		description = "You may play a weapon from your hand as a Parry (unless it is your last card).",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034569/6017AB828EBA4F49347197D5C7EC148750C80AF1/",
	},
	Hideyoshi = {
		name = "hideyoshi",
		health = 4,
		description = "During your draw phase, draw 1 additional card.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034648/D42F57D6B21BFDB9E29CD8A06944EF179C82AC72/",
	},
	Ieyasu = {
		name = "ieyasu", 
		health = 5,
		description = "During your draw phase, you may draw your first card from the top of the discard pile.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070125442/14339E0C5C9F1DAC8D839AEEBF7B0022DE53BE3B/",
	},
	Kojiro = {
		name = "kojio",
		health = 5,
		description = "Your Weapons may attach any Difficulty.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034933/C2796DFE5A689993D435BCBFE1182F668484F97E/",
	},
	Musashi = { 
		name = "musashi",
		health = 5,
		description = "Your Weapons deal 1 additional wound.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035234/9215581396DC45CE6F09B6A399E1FF1FAA3253DA/",
	},
	Nobunaga = {
		name = "nobunaga", 
		health = 5,
		description = "During your turn, you may discard 1 Resilience Point (except your last one) to draw 1 card.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035319/51FF5A59714ECCA69A4981908871680E54BA150B/",
	},
	Tomoe = {
		name = "tomoe",
		health = 5,
		description = "Each time your Weapon deals wounds to a player, draw 1 card from the deck.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035655/DE58A3D217D3254A9F7E4D34BB9FDD7394EFE3C7/",
	},
	Ushiwaka = {
		name = "ushiwaka",
		health = 4, 
		description = "Each time you suffer 1 wound from a Weapon, draw 1 card from the deck.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035728/575D03777AE7363DEBF238BE81FE3E947AF0B4B9/",
	},

	-- Rising Sun Expansion 
	Bokuden = {
		name = "bokuden",
		health = 5, 
		description = "Each time you play a Temple card, you may draw 1 card from the deck.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034048/4B743DB295732DFC67AF379FB96117AA795D08DB/",
	},
	Gracia = {
		name = "gracia",
		health = 4, 
		description = "At the start of your turn, regain 1 Resilience Point.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034443/8EB26CB45A11D64729E3B79849627FEA8A70854E/",
	},
	Kanbei = {
		name = "kanbei",
		health = 5, 
		description = "You may discard one of your Properties in play as a Parry.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034793/3D9F1D79B7C2AB386DA23DF86C72A772178B33D4/",
	},
	Kenshin = {
		name = "kenshin",
		health = 4, 
		description = "Among Weapons, only Origami may wound you.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070034865/0E655CACFDC5FB4FF5BE9A6E9FB930DEF2042B81/",
	},
	["Lady Chacha"] = {
		name = "lady chacha",
		health = 5, 
		description = "When you are the target of a Weapon, reveal 2 cards: if the symbol is the same, you suffer no wounds.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035003/8D8ABCE0E302E294D14C6A6087677D8472247F83/",
	},
	Masamune = {
		name = "masamune",
		health = 6, 
		description = "When revealing, you may reveal up to 3 cards and choose the one to use. (Also works with Bushido.)",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035094/E9DF307013D92B4B7D3916B0B8319C20AAD8C209/",
	},
	Motonari = {
		name = "motonari",
		health = 5, 
		description = "Each time a player loses all their Resilience Points, draw 1 card from the deck.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035164/DE8571676B2F1BDCE51DB29826A0C52F4E87C9AE/",
	},
	Okuni = {
		name = "okuni",
		health = 4, 
		description = "Each time you are the target of an Action, you may play a Origami card to avoid its effect.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035416/CC89AD0D5E6E04A44AE64FA63FEF869770C80B44/",
	},
	Shima = {
		name = "shima",
		health = 4, 
		description = "Once per turn, you may discard 1 Resilience Point (except your last) to take any Property in play.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035502/F14FCBEDA26975E92D7BF1B4BEF6455002D50B5B/",
	},
	Shingen = {
		name = "shingen",
		health = 5, 
		description = "Mountain Weapons do not count for the limit of Weapons you can play.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035592/EB53E513C0B06F76926584CD796939E2377E40AF/",
	},
	Yoshihiro = {
		name = "yoshihiro",
		health = 8, 
		description = "Each time you are wounded by Weapons, you suffer 1 additional wound.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035798/16EBF466E9C29E6BC717FE215F953D32E46B2370/",
	},
	Yukimura = {
		name = "yukimura",
		health = 4, 
		description = "Each time another player wounds you, reveal: Origami = that player also suffers 1 wound.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070035876/B900164AE418800419DD538FE02D2994456BFABE/",
	},
}

role_map = {
	Shogun = {
		name = "shogun",
		stars = 2,
		description = "2 stars",
		color = "yellow",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052390/4C609EB8F741EA92232BD199A9A99D6483FCF078/"
	},
	["Ninja (1)"] = {
		name = "ninja",
		stars = 1,
		description = "1 star",
		color = "blue",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052295/E0E0E458C90305166B5DFF4D09450DFE5364583F/"
	},
	["Ninja (2)"] = {
		name = "ninja",
		stars = 2,
		description = "2 stars",
		color = "blue",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052189/D5280B1BCE46254E0392070466A3FF70F0C1F184/"
	},
	["Samurai (1)"] = {
		name = "samurai",
		stars = 1,
		description = "1 star",
		color = "yellow",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052628/AA8C40F8CFC36522E386321F55A2EF53E730FB06/"
	},
	["Ronin (1)"] = {
		name = "ronin",
		stars = 1,
		description = "1 star",
		color = "red",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052552/3A516EDBAC85BACAC73F20B891D33AC540BC872A/"
	},
	["Ninja (3)"] = {
		name = "ninja",
		stars = 3,
		description = "3 stars",
		color = "blue",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052483/EEC463C81E193949CD6377C03166CBFB47170ABB/"
	},
	["Samurai (3)"] = {
		name = "samurai",
		stars = 3,
		description = "3 stars",
		color = "yellow",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052764/13CC2B70BC53817D382B43A734EFEE095D374639/"
	},
	["Ronin (2)"] = {
		name = "ronin",
		stars = 2,
		description = "2 stars",
		color = "red",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070052696/99D814FDD62A7308ACE5071652D284614EA28578/"
	},
}

play_card_map = {
	-- Props
	Armor = {
		name = "armor",
	 	count = 4,
		symbol = "origami",
		description = "All other players attack you at +1 Difficulty.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070051223/FC5BE338CC2B6A314F6C0184D595F94D700B27B1/",
	},
	["Fast Draw"] = {
		name = "fast draw",
	 	count = 3,
		symbol = "origami",
		description = "Your weapons deal 1 additional wound.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070050959/790C19E828BFBDC0CA62AB93088B9775E3072DE9/",
	},
	Focus = {
		name = "focus",
	 	count = 6,
		symbol = "origami",
		description = "You may play 1 additional Weapon each turn.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070050959/790C19E828BFBDC0CA62AB93088B9775E3072DE9/",
	},
	Bushido = {
		name = "bushido",
	 	count = 2,
		symbol = "origami",
		description = "At your turn, flip 1 card. If a weapon: discard 1 Weapon and pass Bushido, or lose 1 Honor Point and discard Bushido. If not: pass Bushido.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070051121/34728CF39A17D554A9C02507D839BF28F5DF848C/",
	},
	Peasant = {
		name = "peasant",
	 	count = 3,
		symbol = "origami",
		description = "Instead of suffering 1 wound, you may discard Peasant from play.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070050585/B9C58E2B8680A37DC7C0331DEBC81CBF9A26496F/",
	},
	Kote = {
		name = "kote",
	 	count = 1,
		symbol = "origami",
		description = "Each time you play a Koi card, a player of your choice draws 1 card from the deck.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070050508/C1C4C465701B41BD01E40B98FFC44084F8DF0ABA/",
	},
	Attendant = {
		name = "attendant",
	 	count = 1,
		symbol = "origami",
		description = "During your Draw Phase, draw 1 additional card.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070050783/2657881632EE908923CBD2A5D76401988CDE9F83/",
	},
	Curse = {
		name = "curse",
	 	count = 3,
		symbol = "origami",
		description = "When you lose all your Resilience Points, discard all your Properties in play.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070050886/3C17339549AC85A1ABF620563782136E80FB016D/",
	},
	["Bleeding Wound"] = {
		name = "bleeding wound",
	 	count = 2,
		symbol = "origami",
		description = "Discard Bleeding Wound if you lose an Honor Point. On your turn, reveal: Origami = lose 1 Resilience Point (unless it's your last).",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070050703/85453D9DF3294940C6E222BACCD1F03E8827BA54/",
	},

	-- Actions
	Divertion = {
		name = "divertion",
	 	count = 5,
		symbol = "koi",
		description = "Draw 1 random card from the hand of any other player.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070027347/CD3D3A80A035DA53A69B5158E70515D2138816FE/",
	},
	Geisha = {
		name = "geisha",
	 	count = 7,
		symbol = "koi",
		description = "Discard 1 card from play or from the hand of another player.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070027254/D8677ECF7E49C3DA9975926E7379B78F77A807F9/",
	},
	Jujutsu = {
		name = "jujutsu",
	 	count = 3,
		symbol = "mountain",
		description = "All other players choose to discard 1 Weapon or suffer 1 wound.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070027149/FA26F17D529D41EE72732E6F12F6C1AAFB83DA90/",
	},
	Battlecry = {
		name = "jujutsu",
	 	count = 4,
		symbol = "mountain",
		description = "All other players choose to discard 1 Parry or suffer 1 wound.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070027149/FA26F17D529D41EE72732E6F12F6C1AAFB83DA90/",
	},
	Parry = {
		name = "parry",
	 	count = 16,
		symbol = "temple",
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070027586/06D655A4C508D72D7602224EA189483794E629BB/",
	},
	["Concurrent Attack"] = {
		name = "concurrent attack",
	 	count = 2,
		symbol = "origami",
		description = "Discard 1 Resilience Point (not your last) then pick another player to lose 1 Resilience Point (not their last)",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070026992/F1C32B9529EA35D3217348DBF214853A00BFC92A/",
	},
	["Tea Ceremony"] = {
		name = "tea ceremony",
	 	count = 5,
		symbol = "temple",
		description = "Draw 3 cards from the deck. All other players draw 1 card from the deck.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070026646/8519111574849CD0BB785D0FF47D15853714F41D/",
	},
	Daimyo = {
		name = "daimyo",
	 	count = 3,
		symbol = "origami",
		description = "Draw 2 cards from the deck. Awards 1 Honor Point if in hand at game end.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070027066/A99281AE6C50BAF921D518500733D84059CC5000/",
	},
	Breathing = {
		name = "breathing",
	 	count = 3,
		symbol = "temple",
		description = "Regain all your Resilience Points. Another player of your choice draws 1 card from the deck.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070027438/CC2C0E11D4268D9A64663A513CC73E6EA83DC873/",
	},
	Counterattack = {
		name = "counterattack",
	 	count = 2,
		symbol = "temple",
		description = "Counts as Parry against Weapons. The attacker chooses to discard Parry or suffer 1 wound.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070026891/32CCEC6942658D3A64BA606C9C634775E24CED31/",
	},
	Mimicry = {
		name = "mimicry",
	 	count = 3,
		symbol = "origami",
		description = "Take any Property in play.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070026728/40621A2FB831A8190121F881D91EADFEAB54B68E/",
	},
	Intuition = {
		name = "intuition",
	 	count = 3,
		symbol = "koi",
		description = "All non-harmless players show a card. Choose one and take it.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070026828/560EED3C433966785ECCCC69B6CD1DE44C251253/",
	},

	-- Weapons
	Daikyu = {
		name = "daikyu",
	 	count = 1,
		symbol = "mountain",
		difficulty = 5,
		damage = 2,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053437/3236D69823ACF07646D16B6AB0A74E898AD4AC2B/",
	},
	Nodachi = {
		name = "nodachi",
	 	count = 1,
		symbol = "mountain",
		difficulty = 3,
		damage = 3,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053945/A58DF13E85157F21A966A986E89071E2885F6C55/",
	},
	Nagayari = {
		name = "nagayari",
	 	count = 1,
		symbol = "mountain",
		difficulty = 4,
		damage = 2,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053804/E10B8E4FB7DDF05A5083C0B3C8AB665AEC4DEF12/",
	},
	Katana = {
		name = "katana",
	 	count = 1,
		symbol = "mountain",
		difficulty = 2,
		damage = 3,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053591/05DFDC7F2C44DA7D3D62F27716B460103EFE7FB4/",
	},
	Shuriken = {
		name = "shuriken",
	 	count = 3,
		symbol = "origami",
		difficulty = 3,
		damage = 1,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054106/90B51F3D71E96D94349674715FBE2109F63B6E57/",
	},
	Naginata = {
		name = "naginata",
	 	count = 2,
		symbol = "mountain",
		difficulty = 4,
		damage = 1,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053881/CBB5FB64CE91B565038E5D715BF57E10A7A65A16/",
	},
	Bokken = {
		name = "bokken",
	 	count = 6,
		symbol = "origami",
		difficulty = 1,
		damage = 1,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053352/47DD9DFF14A5709928BB07C5883628BEF0BCB39F/",
	},
	Kiseru = {
		name = "kiseru",
	 	count = 5,
		symbol = "origami",
		difficulty = 1,
		damage = 2,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053661/29D58E7174F50692C66C6BF042539B3D70DF2FF8/",
	},
	Bo = {
		name = "bo",
	 	count = 5,
		symbol = "origami",
		difficulty = 2,
		damage = 1,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053284/7FB40B02936F4E3B174E55AD3E72DB059C84795C/",
	},
	Kusanagi = {
		name = "kusanagi",
	 	count = 1,
		symbol = "mountain",
		difficulty = 3,
		damage = 0,
		description = "Deals 1 wound for each Honor Point of the wounded player.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054675/47FF23C0D577617F4104B00255ED01D7A17E9539/",
	},
	Kozuka = {
		name = "kozuka",
	 	count = 1,
		symbol = "origami",
		difficulty = 1,
		damage = 1,
		description = "If it deals any wound, take it back in your hand immediately.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054595/AE6F505A783B84DBC5423E3B689F79E9B1CB485D/",
	},
	Tanto = {
		name = "tanto",
	 	count = 1,
		symbol = "mountain",
		difficulty = 1,
		damage = 1,
		description = "You may play it on yourself: if you lose your last Resilience Point, gain 1 Honor Point and end your turn.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054957/BB926F0B55E51B08588D92C163BBFB020078F696/",
	},
	Kusarigama = {
		name = "kusarigama",
	 	count = 4,
		symbol = "origami",
		difficulty = 2,
		damage = 2,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053728/E00798D866FB61CE288B144A4F3BB6C5A1F76572/",
	},
	Wakizashi = {
		name = "wakizashi",
	 	count = 1,
		symbol = "mountain",
		difficulty = 1,
		damage = 3,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054266/46C7DFE00F0AE1B98136AFC85D8B5ADBEA614A10/",
	},
	Kanabo = {
		name = "kanabo",
	 	count = 1,
		symbol = "origami",
		difficulty = 3,
		damage = 2,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070053520/021706DBAFEFD072912D87583CDD35BABBB953B1/",
	},
	Jitte = {
		name = "jitte",
	 	count = 2,
		symbol = "temple",
		difficulty = 1,
		damage = 2,
		description = "May be played as Parry",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054530/CA16FD43D4674F32FD9ABD0AE24828AADD6BA732/",
	},
	Zen = {
		name = "zen",
	 	count = 3,
		symbol = "mountain",
		difficulty = 9,
		damage = 0,
		description = "Deals 1 wound for each Property in play in front of the wounded player.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054340/948B2E1B4705E0AD427F944105268484B47C1273/",
	},
	Tessen = {
		name = "tessen",
	 	count = 2,
		symbol = "temple",
		difficulty = 2,
		damage = 1,
		description = "May be played as Parry",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070055030/5C5722FEF0F0905F4DA14AD32C5B6F43E8146EEE/",
	},
	Chigiriki = {
		name = "chigiriki",
	 	count = 2,
		symbol = "origami",
		difficulty = 2,
		damage = 2,
		description = "To parry, the target must use a Weapon instead of Parry",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054437/85CE869B6A6F85E20F1CEE1199CC5B9105BC2948/",
	},
	Tanegashima = {
		name = "tanegashima",
	 	count = 1,
		symbol = "mountain",
		difficulty = 5,
		damage = 1,
		description = "",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054180/63A7FC85AB209C092BEE593058EA142C598C0B8B/",
	},
	Makibishi = {
		name = "makibishi",
	 	count = 2,
		symbol = "origami",
		difficulty = 1,
		damage = 1,
		description = "May be played to target players with no cards in hand.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054739/D86EC710346AAF8EBC79113F2D838824D273A95D/",
	},
	Shigehtoyumi = {
		name = "shigehtoyumi",
	 	count = 1,
		symbol = "mountain",
		difficulty = 9,
		damage = 1,
		description = "You may play an additional Weapon after this one.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054885/D303789FBC157AF08DB03FE6D7145A2105A48EAD/",
	},
	Manrikigusari = {
		name = "manrikigusari",
	 	count = 2,
		symbol = "origami",
		difficulty = 3,
		damage = 1,
		description = "If it deals any wound, the wounded player must choose and discard 1 card from their hand.",
		face_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070054811/A4616A805CC4BA46622C7F7B176FE3D666BD627D/",
	},
}

-- Card backs
character_back_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070032721/2ED88040D29D832B7EEFD33DE74B5F4EF44D70FB/"
role_back_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070032633/F888F6F1E52949973347541B7F5610F052F29569/"
card_back_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070032560/A38870FCECFB7F29C9EF3608AEA70A40167637EB/"

-- if we are good at coding we won't need to spawn this in ;)
score_card_path = "http://cloud-3.steamusercontent.com/ugc/2017092949070032462/04B20D26506EDB3315DF419A5402FA410450168E/"

-- Card Symbols
card_symbol_map = {
	"mountain",
	"koi",
	"temple",
	"origami",
}

-- Commands
command_map = {
	"\\start : Start the game",
	"\\character : List each character being played and by whom",
	"\\help : list these commands (again)"
}


